#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import regex
import codecs
import getopt

### tylko znaki zawarte w liscie alfabet beda kodowane ###
### wszystkie inne pozostaja takie jak w oryginalnej wiadomosci ###
polski = [u'a', u'ą', u'b', u'c', u'ć', u'd', u'e', u'ę', u'f', u'g', u'h', u'i', u'j', u'k', u'l', u'ł', u'm', u'n', u'ń', u'o', u'ó', u'p', u'r', u's',u'ś', u't', u'u', u'w', u'y', u'z', u'ź', u'ż' ]

#test przykladu z polskiej wiki
lacinski = [u'a', u'b', u'c', u'd', u'e', u'f', u'g', u'h', u'i', u'j', u'k', u'l', u'm', u'n', u'o', u'p', u'q', u'r', u's', u't', u'u', u'v', u'w', u'x', u'y', u'z']

class devigener(object):
	def __init__(self, alfabet):
		self.alfabet=alfabet
                self.tablica={x:shift(alfabet,number) for number,x in enumerate(alfabet)}
                self.numeracja={a:x for x,a in enumerate(alfabet)}

	def wygeneruj_tekst(self, tekst, klucz):
		'''
		analogicznie jak przy kodowaniu kazdej literze w zagodowanym hasle przypozadkujemy wartosc slowa klucz			
		'''
		self.tekst=regex.findall(u'\X', tekst)
                self.klucz=klucz
		self.kodowanie=[]
		start=0
		dlugosc=len(self.klucz)
		for char in self.tekst:
			if start >= dlugosc:
                                start = 0
                        if char.islower():
                                if char in self.alfabet:
					self.kodowanie.append(self.klucz[start])
        	                        start+=1
				else:
					self.kodowanie.append(char)
                        else:
                                if char.lower() in self.alfabet:
                                        self.kodowanie.append(self.klucz[start].upper())
                                        start+=1
                                else:
                                       	self.kodowanie.append(char)
                return self
	def decode_tekstem(self):
		# w sumie pewnie latwiej bylo wziasc zakodowany znak i cofnac ja o X gdzie X jest przesunieciem z klucza ...
		# zaimplementowano w decode_liczbami wersje te zostawiam wiec jako alternatywe do przemyslen
		self.oryginal=[]
		for number in range(len(self.kodowanie)):
			if self.kodowanie[number].islower():		
				if self.kodowanie[number] in self.alfabet:
					element_klucza=self.kodowanie[number]
					element_kodujacy=self.tekst[number]
					numeracja_klucza=self.numeracja[element_klucza] 
					for klucz in self.tablica.keys():
						if self.tablica[klucz][numeracja_klucza]==element_kodujacy:
							self.oryginal.append(klucz)
				else:
					self.oryginal.append(self.kodowanie[number])
			else:
				if self.kodowanie[number].lower() in self.alfabet:
					element_klucza=self.kodowanie[number].lower()
	                                element_kodujacy=self.tekst[number].lower()
        	                        numeracja_klucza=self.numeracja[element_klucza]
                	                for klucz in self.tablica.keys():
                        	                if self.tablica[klucz][numeracja_klucza]==element_kodujacy:
                                	                self.oryginal.append(klucz.upper())
				else:
					self.oryginal.append(self.kodowanie[number])
		return self
	def dekodowanie_liczbami(self):	
		'''
		podobnie jak przy kodowaniu musimy przejsc z systemu 1-owego (uzytkownika) na 0-owy (pythona)
		'''
		self.oryginal=[]
                for number in range(len(self.kodowanie)):
                        if self.tekst[number].islower():
                       		#powiedzmy ze w kodzie mamy literke B ktora jest 2 literka alfabetu
				numer=self.numeracja[self.tekst[number]]
				#i zostala zakodowana alfabetem przesunietym o 3 (o tym de facto mowi klucz)
				przesuniecie=self.kodowanie[number]
				# no to prawdziwa wiadomosc to numer - przesuniecie
				self.oryginal.append(self.alfabet[int(numer)-int(przesuniecie)+1])
			else:
				if self.tekst[number].lower() in self.alfabet:
					numer=self.numeracja[self.tekst[number].lower()]
					przesuniecie=self.kodowanie[number]
					self.oryginal.append(self.alfabet[int(numer)-int(przesuniecie)+1].upper())
                                else:
                                        self.oryginal.append(self.kodowanie[number])

		return self	
	def __str__(self):
		try:
                        return "".join(self.oryginal).encode('utf-8')
                except:
                        return "No text has been decoded\n"

class vigener(object):
	def __init__(self, alfabet):
		self.alfabet=alfabet
		self.tablica={x:shift(alfabet,number) for number,x in enumerate(alfabet)}
		self.numeracja={a:x for x,a in enumerate(alfabet)}

	def wygeneruj_tekst(self, tekst, klucz):
		'''
		podmieniamy tekst na wystapienie slowa kluczu i trzymamy w self.kodowanie
		'''
		self.tekst=regex.findall(u'\X', tekst)
		self.klucz=klucz
		dlugosc=len(self.klucz)
		start=0
		self.kodowanie=[]
		for char in self.tekst:
			if start >= dlugosc:
				start = 0
			if char.islower():
				if char in self.alfabet:
					self.kodowanie.append(self.klucz[start])
					start+=1
				else:
					self.kodowanie.append(char)
			else:
				if char.lower() in self.alfabet:
                                       	self.kodowanie.append(self.klucz[start].upper())
                                    	start+=1
                                else:
                                       	self.kodowanie.append(char)
		return self
	def szyfrowanie_tekstem(self):
		self.zaszyfrowana=[] # tu trzymamy zaszyfrowana wiadomosc 
		for number in range(len(self.kodowanie)): # no to lecimy po wiadomosci
			if self.kodowanie[number].islower():
				if self.kodowanie[number] in self.alfabet:
					self.zaszyfrowana.append(self.tablica[self.tekst[number]][self.numeracja[self.kodowanie[number]]]) 
				### zalozmy ze literze B z oryginalnej wiadomosci odpowiada A z klucza 
				### self.kodowanie[number] zwraca znak 'A' , a numeracja['A'] da nam info ze A jest w alfabecie na pozycji 0 
				### self.tekst[number] bierze powiedzmy 5 znak z oryginalnej wiadomosci (powiedzmy B)B nastepnie patrzymy jakie litera B ma kodowanie w self.tablica
				### wiemy wiec ze 0 element w kodowaniu dla B to rowniez B wiec zaszyfrowana wiadomosc ma "B"
				else:
					self.zaszyfrowana.append(self.tekst[number])
			else:
				if self.kodowanie[number].lower() in self.alfabet:
					self.zaszyfrowana.append(self.tablica[self.tekst[number].lower()][self.numeracja[self.kodowanie[number].lower()]].upper())
				else:
					self.zaszyfrowana.append(self.tekst[number])
		return self
	def szyfrowanie_liczbami(self):
		'''
		gdyby uzytkownik zamiasat slowa podal ciag liczb w kluczu
		zakladamy ze uzytkownik posluguje sie systemem od 1 do n (gdzie 1 oznacza litere a i de facto brak przesuniecia)
		'''
		self.zaszyfrowana=[]
		for number in range(len(self.kodowanie)): # no to lecimy po wiadomosci
                        if self.tekst[number].islower():
				if self.tekst[number] in self.alfabet:
					self.zaszyfrowana.append(self.tablica[self.tekst[number]][int(self.kodowanie[number])-1])
				else:
					self.zaszyfrowana.append(self.tekst[number])
			else:
				if self.tekst[number].lower() in self.alfabet:
					self.zaszyfrowana.append(self.tablica[self.tekst[number].lower()][int(self.kodowanie[number])-1].upper())
				else:
					self.zaszyfrowana.append(self.tekst[number])
		return self
	
	def __str__(self):
		try:
			return "".join(self.zaszyfrowana).encode('utf-8')
		except:
			return "No text has been encrypted\n"

def shift(lista, n):
	return lista[n:]+lista[:n]

def menu(argv):
	tekt=''
	klucz=''
	code=0
	decode=0
	numeric=0
	typ_alfabetu='polski'
	if len(argv) == 0:
		argv=["-h"]
	try:
		opts,args = getopt.getopt(argv, "i:k:a:cdnh", ["input=", "key=", "alfabet=", "code", "decode",  "numeric", "help"])
	except getopt.GetoptError:
		print "Use --help or -h for help" 
		sys.exit(2)
	for opt, arg in opts:
		if opt in ("-h", "--help"):
			print "Skrypt rozumie nastepujace argument:\
\n-i plik z tekstem do zakodowania/dekodowania\
\n-k plik z kluczem\
\n-c kodowanie tekstu\
\n-d dekodowanie tekstu\
\n-a alfabet [rozumiane wartosci to polski i lacinski]( default polski)\
\n-n uzyj jesli podany klucz jest numeryczny [defaultowo klucz jest tekstem, przy kluczu liczbowym kolejne liczby muszą byc rozdzielone spacjami]"
			sys.exit(2)
		elif opt in ("-i", "--input"):
			tekst=arg
		elif opt in ("-k","--key"):
			klucz=arg
		elif opt in ("-c","--code"):
			code=1
		elif opt in ("-d","--decode"):
                        decode=1
		elif opt in ("-a","--alfabet"):
                        typ_alfabetu=arg
		elif opt in ("-n", "--numeric"):
			numeric=1

	return  tekst,klucz,code,decode,numeric,typ_alfabetu

if __name__=="__main__":
	# read args
	arg=menu(sys.argv[1:])

	### typ alfabetu ##
	if "polski" in arg[5]:
		alfabet=polski
	elif "lacinski" in arg[5]:
		alfabet=lacinski

	#read tekst from file
	msg="".join(codecs.open(arg[0], encoding='utf-8').readlines())
	if int(arg[4]):
		msg2=open(arg[1]).readlines()
		msg3=[]
		for  x in msg2:
			msg3.extend(x.rsplit())
		msg2=msg3
	else:
		msg2="".join(codecs.open(arg[1], encoding='utf-8').readlines()).replace("\n","")
	if int(arg[2]) and not int(arg[4]):
		ola=vigener(alfabet)
		ola.wygeneruj_tekst(msg,msg2)
		ola.szyfrowanie_tekstem()
		print ola
	elif int(arg[2]) and int(arg[4]):
		ala=vigener(alfabet)
		ala.wygeneruj_tekst(msg,msg2)
		ala.szyfrowanie_liczbami()
		print ala
	elif int(arg[3]) and not int(arg[4]):
		ola=devigener(alfabet)
		ola.wygeneruj_tekst(msg,msg2)
                ola.decode_tekstem()
                print ola

	elif int(arg[3]) and int(arg[4]):
                ola=devigener(alfabet)
                ola.wygeneruj_tekst(msg,msg2)
                ola.dekodowanie_liczbami()
                print ola
	else:
		print "Nie wybrano opcji czy tekst ma zostac zakodowany czy dekodowany, uzyj -h aby wyswietlic argumenty\n"
		sys.exit(2)
